document.querySelector('#searchArea').addEventListener('submit', function(e){
    e.preventDefault();
});
var load = function()
{
    //getFiles trebuie sa ia ca prim paramentru url dorit la cautare
    getFiles('', 'staticResponse.html', function(s)
    {
        document.getElementById('contentArea').innerHTML = s;
    });
};

var display = function(par){
    if(par != 'html'){
        document.getElementById('json').style.display = 'block';
        document.getElementById('html').style.display = 'none';
        document.getElementById('json-btn').className="btn active";
        document.getElementById('html-btn').className = 'btn';
    }
    else{
        document.getElementById('json').style.display = 'none';
        document.getElementById('html').style.display = 'block';
        document.getElementById('json-btn').className="btn";
        document.getElementById('html-btn').className = 'btn active';
    }
};


var getFiles = function(search, name, callback){
    var client = new XMLHttpRequest();
    client.open('GET', './pages/' + name);
    client.onreadystatechange = function(){
        if(search)
            requestExample(search, function(s){
                callback(client.responseText + s);
            });
        else
            callback(client.responseText);
    };
    client.send();
};

var requestExample = function(url, callback){
    var xhr = new XMLHttpRequest(),
        method = "GET";

    xhr.open(method, url, true);
    xhr.onreadystatechange = function (){
        if(xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
            console.log(xhr.responseText);
            callback(xhr.responseText);
        }
    };
    xhr.send();
};
var googleRequest = function(input){
    var req = new XMLHttpRequest();
    req.open('GET','https://www.google.ro/search?q=' + input);
    req.send()
};

var config = getConfig();

var populateComponents = function(){
    config.types.forEach(function (value)
    {
        document.getElementById('category').innerHTML += '<li><input id="' + value + '" type="checkbox" name="'+ value + '"> ' + value + '</li>';
    });
};
populateComponents(config);